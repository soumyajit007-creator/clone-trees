#!/bin/bash

   #remove_HALS
     #rm -rf hardware/qcom-caf/wlan
     #rm -rf hardware/qcom-caf/msm8996/audio
     #rm -rf hardware/qcom-caf/msm8996/display
     #rm -rf hardware/qcom-caf/msm8996/media
   
   #remove_old_trees
     rm -rf device/xiaomi/onclite
     #rm -rf vendor/xiaomi/onclite
     #rm -rf kernel/xiaomi/onclite 
   
   #clone_HALS
     #git clone https://github.com/soumyajit007-creator/android_hardware_qcom_display -b lineage-18.1-caf-msm8996 hardware/qcom-caf/msm8996/display
     #git clone https://github.com/LineageOS/android_hardware_qcom_audio.git -b lineage-18.1-caf-msm8996 hardware/qcom-caf/msm8996/audio
     #git clone https://github.com/soumyajit007-creator/android_hardware_qcom_media -b lineage-18.1-caf-msm8996 hardware/qcom-caf/msm8996/media
     #git clone https://github.com/soumyajit007-creator/hardware_qcom-caf_wlan -b twelve hardware/qcom-caf/wlan
    
    #clone_trees
     git clone https://github.com/soumyajit007-creator/device_xiaomi_onclite -b twelve device/xiaomi/onclite
     git clone https://github.com/soumyajit007-creator/vendor_xiaomi_onclite -b twelve vendor/xiaomi/onclite
     git clone https://github.com/dotOS-Devices/kernel_xiaomi_onclite kernel/xiaomi/onclite

     #rm -rf prebuilts/clang/host/linux-x86/clang-proton
     #git clone https://github.com/kdrag0n/proton-clang --depth=1 prebuilts/clang/host/linux-x86/clang-proton
